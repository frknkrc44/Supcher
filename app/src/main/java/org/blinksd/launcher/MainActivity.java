package org.blinksd.launcher;

import android.animation.*;
import android.app.*;
import android.content.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import org.blinksd.launcher.modules.*;
import org.blinksd.launcher.modules.lockscreen.*;

public class MainActivity extends Activity {
	
	PackageList pl;
	SharedPreferences sp;
	SharedPreferences.Editor se;
	ScrollView al;
	Commons cm;
	boolean lock,hideui = false;
	ManageApp mgapp;
	FullScreen fs;
	ClockWidget cw;
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		pl = new PackageList(this);
		sp = getSharedPreferences("settings", MODE_PRIVATE);
		se = sp.edit();
		cm = new Commons(this);
		mgapp = new ManageApp();
		fs = new FullScreen(this);
		cw = new ClockWidget(this);
		setContentView(main());
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
								WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		handler();
    }
	
	LinearLayout clock;
	
	LinearLayout main(){
		final LinearLayout ml = new LinearLayout(this);
		ml.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   LinearLayout.LayoutParams.MATCH_PARENT));
		ml.setBackgroundColor(0x66000000);
		ml.setOrientation(LinearLayout.VERTICAL);
		ml.setGravity(Gravity.CENTER_HORIZONTAL);
		ml.setPadding(0,getStatusBarHeight(),0,getNavBarHeight());
		ml.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				lock();
			}
		});
		clock = sp.getBoolean("binaryClk",false) ? cw.binaryClock() : cw.analogClock();
		ml.addView(clock);
		/*h.post(new Runnable(){
			public void run(){*/
				al = appList();
				/*
				h.postDelayed(new Runnable(){
					public void run(){
						if(hideui){
							al.setVisibility(View.GONE);
							al.animate().translationY(-(cm.getDeviceWidth()/2.4f)).scaleX(0).scaleY(0).setDuration(1);
							clock.animate().translationY(cm.getDeviceWidth()/2.4f).scaleX(2).scaleY(2).setDuration(1);
						}
					}
				},25);*/
				clock.setOnLongClickListener(new View.OnLongClickListener(){
						public boolean onLongClick(View v){
							if(clock.getTranslationY() >= (cm.getDeviceHeight(false)/3)) 
								clock.animate().translationY(0).scaleY(1).scaleX(1).
								setListener(new Animator.AnimatorListener(){

										@Override
										public void onAnimationStart(Animator p1){
											clock.setEnabled(false);
										}

										@Override
										public void onAnimationEnd(Animator p1){
											al.animate().scaleY(1).scaleX(1).translationY(0).
												setListener(new Animator.AnimatorListener(){
													@Override
													public void onAnimationStart(Animator p1){
														hideui = false;
														al.setVisibility(View.VISIBLE);
														fs.showSystemUI();
													}

													@Override
													public void onAnimationEnd(Animator p1){
														clock.setEnabled(true);
														h.postDelayed(rx,10);
													}

													@Override
													public void onAnimationCancel(Animator p1){}

													@Override
													public void onAnimationRepeat(Animator p1){}
												});
										}

										@Override
										public void onAnimationCancel(Animator p1){}

										@Override
										public void onAnimationRepeat(Animator p1){}
									});
							else {
								se.putBoolean("binaryClk",!sp.getBoolean("binaryClk",false));
								se.commit();
								setContentView(main());
							}
							return false;
						}
					});
				clock.setOnClickListener(new View.OnClickListener(){
						public void onClick(View v){
							if (clock.getTranslationY() <= (cm.getDeviceHeight(false)/3)){
								if(lock) lock();
								else al.animate().scaleY(0).scaleX(0).
										translationY(-(cm.getDeviceHeight(false)/2.4f)).
										setListener(new Animator.AnimatorListener(){
											@Override
											public void onAnimationStart(Animator p1){
												clock.setEnabled(false);
												hideui = true;
											}

											@Override
											public void onAnimationEnd(Animator p1){
												clock.animate().translationY(cm.getDeviceHeight(false)/2.5f).scaleY(2).scaleX(2);
												al.setVisibility(View.GONE);
												al.scrollTo(0,0);
												clock.setEnabled(true);
											}

											@Override
											public void onAnimationCancel(Animator p1){

											}

											@Override
											public void onAnimationRepeat(Animator p1){

											}
										});
							} else lock();
						}
					});
				ml.addView(al);
			/*}
		});*/
		return ml;
	}
	
	public int getStatusBarHeight(){ 
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		return resourceId > 0 ? getResources().getDimensionPixelSize(resourceId) : 0;
	}
	
	public int getNavBarHeight(){ 
		int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
		return resourceId > 0 ? getResources().getDimensionPixelSize(resourceId) : 0;
	}
	
	ScrollView appList(){
		if(sp.getString("ACTION","").equals(Intent.ACTION_ALL_APPS)){
			pl = new PackageList(this);
			se.remove("ACTION");
			se.commit();
		}
		final ScrollView sc = new ScrollView(this);
		sc.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   LinearLayout.LayoutParams.MATCH_PARENT));
		sc.setOverScrollMode(View.OVER_SCROLL_NEVER);
		LinearLayout lv = new LinearLayout(this);
		lv.setLayoutParams(new ScrollView.LayoutParams(
							   ScrollView.LayoutParams.MATCH_PARENT,
							   ScrollView.LayoutParams.MATCH_PARENT));
		lv.setOrientation(LinearLayout.VERTICAL);
		String[] pkg = pl.getAllLaunchableAppNames();
		Drawable[] ico = pl.getAllLaunchableAppIcons();
		String[] pnm = pl.getAllLaunchableAppPackageNames();
		String[] inn = pl.getAllLaunchableAppPackageIndexNumbers();
		for(int g = 0;g != pkg.length;g+=5){
			LinearLayout ll = new LinearLayout(this);
			ll.setLayoutParams(new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.MATCH_PARENT,
									LinearLayout.LayoutParams.WRAP_CONTENT));
			int h = 0;
			try {
				for(int i = 0;i != 5;i++){
					h = i;
					ll.addView(appListItem(pkg[i+g],ico[i+g],pnm[i+g],inn[i+g]));
				}
				lv.addView(ll);
			} catch(Exception e){
				for(;h != 5;h++){
					View v = new View(this);
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						cm.launcherItemSize(),
						(int)(cm.launcherItemSize()/(cm.getDensity()/2.5f)));
					lp.weight = 0.2f;
					v.setLayoutParams(lp);
					ll.addView(v);
				}
				lv.addView(ll);
				break;
			}
		}
		sc.addView(lv);
		return sc;
	}
	
	LinearLayout appListItem(final String name, Drawable icon, final String pkg, final String index){
		LinearLayout ll = new LinearLayout(this);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			cm.launcherItemSize(),
			LinearLayout.LayoutParams.WRAP_CONTENT);
		lp.weight = 0.2f;
		ll.setLayoutParams(lp);
		ll.setPadding(16,16,16,16);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setGravity(Gravity.CENTER);
		ImageView iv = new ImageView(this);
		iv.setLayoutParams(new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								(int)(cm.launcherItemSize()/1.75)));
		iv.setImageDrawable(icon);
		iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
		iv.setPadding(16,16,16,16);
		TextView tv = new TextView(this);
		tv.setLayoutParams(new LinearLayout.LayoutParams(
								LinearLayout.LayoutParams.MATCH_PARENT,
								LinearLayout.LayoutParams.MATCH_PARENT));
		tv.setText(name);
		tv.setTextSize(cm.textSize3());
		tv.setMinEms(2);
		tv.setMinLines(2);
		tv.setMaxEms(2);
		tv.setMaxLines(2);
		tv.setTextColor(0xFFFFFFFF);
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		tv.setTypeface(cm.tf());
		ll.addView(iv);
		ll.addView(tv);
		ll.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v){
				startActivity(new Intent(getPackageManager().
								getLaunchIntentForPackage(pkg)));
			}
		});
		ll.setOnLongClickListener(new View.OnLongClickListener(){
			@Override
			public boolean onLongClick(View p1){
				
				View[] items1 = {
					cm.selectionItem("Info", R.drawable.info, 
						new View.OnClickListener(){
							public void onClick(View v){
								try {
									startActivity(new Intent(android.provider.Settings.
															 ACTION_APPLICATION_DETAILS_SETTINGS).
												  setData(Uri.parse("package:" + pkg)));
								} catch ( ActivityNotFoundException e ) {
									startActivity(new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS));
								} cm.getCurrentDialog().cancel();
							}
						}),
					cm.selectionItem("Share", R.drawable.share, 
						new View.OnClickListener(){
							public void onClick(View v){
								try {
									pl.pullAndSendAPK(pkg);
								} catch(Exception e){
									Toast.makeText(getBaseContext(),e.toString(),1000).show();
								} cm.getCurrentDialog().cancel();
							}
						}),
					cm.selectionItem("Freeze (root)", R.drawable.freeze, 
						new View.OnClickListener(){
							public void onClick(View v){
								try {
									mgapp.disable(pkg);
								} catch(Exception e){
									Toast.makeText(getBaseContext(),e.toString(),1000).show();
								} cm.getCurrentDialog().cancel();
							}
						}),
					cm.selectionItem("Remove", R.drawable.remove, 
						new View.OnClickListener(){
							public void onClick(View v){
								startActivity(new Intent(Intent.ACTION_DELETE, Uri.fromParts("package",pkg,null)));
								cm.getCurrentDialog().cancel();
							}
						})
				};
				
				View[] items2 = {
					cm.selectionItem("Info", R.drawable.info, 
						new View.OnClickListener(){
							public void onClick(View v){
								try {
									startActivity(new Intent(android.provider.Settings.
													ACTION_APPLICATION_DETAILS_SETTINGS).
													setData(Uri.parse("package:" + pkg)));
								} catch ( ActivityNotFoundException e ) {
									startActivity(new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS));
								} cm.getCurrentDialog().cancel();
							}
						}),
					cm.selectionItem("Freeze (root)", R.drawable.freeze, 
						new View.OnClickListener(){
							public void onClick(View v){
								try { mgapp.disable(pkg); } catch(Exception e){}
								cm.getCurrentDialog().cancel();
							}
						}),
				};
				
				cm.createSelectionDialog(name, pl.getPackage(Integer.parseInt(index)).applicationInfo.
											publicSourceDir.startsWith("/system/") ?  items2 : items1,
											new View.OnClickListener(){
												public void onClick(View v){
													cm.getCurrentDialog().cancel();
												}
											}
										);
				return false;
			}
		});
		return ll;
	}
	
	Handler h = new Handler();
	Runnable r = new Runnable(){
		public void run(){
			if(sp.getString("ACTION","").equals(Intent.ACTION_ALL_APPS))
				setContentView(main());
			if(hideui) fs.hideSystemUI();
			else fs.showSystemUI();
			handler();
		}
	};
	
	Runnable rx = new Runnable(){
		public void run(){
			setContentView(main());
		}
	};
	
	void handler(){
		h.postDelayed(r,500);
	}
	
	void lock(){
		if(lock){
			lock = false;
			startActivity(new Intent(MainActivity.this,LockScreen.class));
		} else {
			lock = true;
			h.postDelayed(new Runnable(){
					public void run(){
						lock = false;
					}
				},500);
		}
	}
	
	@Override
	public void onBackPressed(){
		//super.onBackPressed();
	}
	
}
