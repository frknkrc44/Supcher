package org.blinksd.launcher.modules;

import java.io.*;

public class Shell {
	public int exec(int shellType,String command) throws IOException {
		String st = new String();
		if (shellType == 0) st = "sh";
		if (shellType == 1) st = "su";
		if (shellType == 2) st = "bash";
		if (shellType > 2 || shellType < 0) return 1;
		exec(st,command);
		return 0;
	}

	public int exec(String customShell,String command) throws IOException {
		if (customShell != "" && customShell != null && command != "" && command != null) {
			java.lang.Process p = Runtime.getRuntime().exec(customShell);
			DataOutputStream dos = new DataOutputStream(p.getOutputStream());
			dos.writeBytes(command+"\n");
			dos.flush();
			dos.close();
			return 0;
		} else return 1;
	}
}
