package org.blinksd.launcher.modules.lockscreen;

import android.app.*;
import android.app.admin.*;
import android.content.*;
import android.os.*;

public class LockScreen extends Activity{
	
	private ComponentName componentName = null;
    private DevicePolicyManager policyManager = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		lock();
	}
	
	private void lock(){
		policyManager = (DevicePolicyManager) getSystemService("device_policy");
		componentName = new ComponentName(this, Admin.class);
        if(policyManager.isAdminActive(componentName))
			policyManager.lockNow();
		else startActivity(new Intent("android.app.action.ADD_DEVICE_ADMIN").
						   putExtra("android.app.extra.DEVICE_ADMIN", componentName));
		finish();
	}
	
}
