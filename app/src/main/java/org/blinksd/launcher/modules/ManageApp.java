package org.blinksd.launcher.modules;

public class ManageApp {
	
	Shell sh;
	
	public ManageApp(){
		sh = new Shell();
	}
	
	public void disable(String pkg) throws java.io.IOException {
		sh.exec(1,"pm disable "+pkg);
	}
	
	public void enable(String pkg) throws java.io.IOException {
		sh.exec(1,"pm enable "+pkg);
	}
}
