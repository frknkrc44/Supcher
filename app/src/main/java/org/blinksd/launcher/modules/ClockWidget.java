package org.blinksd.launcher.modules;

import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import java.text.*;
import java.util.*;
import org.blinksd.launcher.*;

public class ClockWidget {
	
	Context c;
	Commons cm;
	
	public ClockWidget(Context ctx){
		c = ctx;
		cm = new Commons(ctx);
	}
	
	public LinearLayout digitalClock(){
		final LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setPadding(cm.menuItemPadding()*2,0,cm.menuItemPadding()*2,cm.menuItemPadding()*2);
		final TextClock tc = new TextClock(c);
		tc.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		tc.setTextSize(cm.menuItemSize()/2);
		tc.setTextColor(0xFFFFFFFF);
		tc.setFormat12Hour("h:mm");
		tc.setFormat24Hour("HH:mm");
		tc.setTypeface(cm.tf());
		final TextView tv = new TextView(c);
		tv.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		tv.setText(sdf.format(new Date()));
		tv.setTextColor(0xFFFFFFFF);
		tv.setTextSize(cm.textSize2());
		tv.setTypeface(cm.tf());
		tc.addTextChangedListener(new TextWatcher(){
			@Override
			public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4){}

			@Override
			public void onTextChanged(CharSequence p1, int p2, int p3, int p4){
				tv.setText(sdf.format(new Date()));
			}

			@Override
			public void afterTextChanged(Editable p1){}
		});
		ll.addView(tc);
		ll.addView(tv);
		return ll;
	}
	
	public LinearLayout binaryClock(){
		LinearLayout ml = new LinearLayout(c);
		ml.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ml.setOrientation(LinearLayout.VERTICAL);
		ml.setGravity(Gravity.CENTER_HORIZONTAL);
		ml.setPadding(0,cm.menuItemPadding()*2,0,cm.menuItemPadding()*2);
		final LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);
		final TextClock tc = new TextClock(c);
		tc.setLayoutParams(new LinearLayout.LayoutParams(1,1));
		ll.addView(tc);
		tc.setFormat12Hour("h:mm");
		tc.setFormat24Hour("HH:mm");
		final TextView tv = new TextView(c);
		tv.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		tv.setText(sdf.format(new Date()));
		tv.setTextColor(0xFFFFFFFF);
		tv.setTextSize(cm.textSize2());
		tv.setTypeface(cm.tf());
		tv.setPadding(0,cm.menuItemPadding(),0,0);
		tc.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4){}

				@Override
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4){
					ll.removeAllViews();
					String[] x = tc.getText().toString().split(":");
					for(int i = 0;i != x.length;i++){
						String[] y = x[i].split("");
						for(int g = 0;g != y.length;g++){
							ll.addView(binaryDigit(y[g]));
							if(g != y.length-1){
								View v = new View(c);
								v.setLayoutParams(new LinearLayout.LayoutParams(
													  cm.menuItemPadding(),
													  cm.menuItemPadding()));
								ll.addView(v);
							}
						}
						if(i != x.length-1){
							View v = new View(c);
							v.setLayoutParams(new LinearLayout.LayoutParams(
												  cm.menuItemPadding()*3,
												  LinearLayout.LayoutParams.MATCH_PARENT));
							ll.addView(v);
						}
					}
					tv.setText(sdf.format(new Date()));
					h.postDelayed((r = new Runnable(){
									  public void run(){
										  ll.addView(tc);
									  }
								  }),100);
				}

				@Override
				public void afterTextChanged(Editable p1){}
			});
		ml.addView(ll);
		ml.addView(tv);
		return ml;
	}
	
	LinearLayout binaryDigit(String number){
		final LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);
		ll.setOrientation(LinearLayout.VERTICAL);
		Drawable d1 = cm.createRadiusDrawable(0x44FFFFFF,100f);
		Drawable d2 = cm.createRadiusDrawable(0xFFFFFFFF,100f);
		int size = (int)(cm.menuItemSize()/2.75f);
		ImageView iv1 = new ImageView(c);
		iv1.setLayoutParams(new LinearLayout.LayoutParams(
								size,size));
		iv1.setScaleType(ImageView.ScaleType.FIT_CENTER);
		ImageView iv2 = new ImageView(c);
		iv2.setLayoutParams(new LinearLayout.LayoutParams(
								size,size));
		iv2.setScaleType(ImageView.ScaleType.FIT_CENTER);
		ImageView iv3 = new ImageView(c);
		iv3.setLayoutParams(new LinearLayout.LayoutParams(
								size,size));
		iv3.setScaleType(ImageView.ScaleType.FIT_CENTER);
		ImageView iv4 = new ImageView(c);
		iv4.setLayoutParams(new LinearLayout.LayoutParams(
								size,size));
		iv4.setScaleType(ImageView.ScaleType.FIT_CENTER);
		switch(number){
			case "0":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d1);
				break;
			case "1":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d2);
				break;
			case "2":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d2);
				iv4.setImageDrawable(d1);
				break;
			case "3":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d2);
				iv4.setImageDrawable(d2);
				break;
			case "4":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d2);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d1);
				break;
			case "5":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d2);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d2);
				break;
			case "6":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d2);
				iv3.setImageDrawable(d2);
				iv4.setImageDrawable(d1);
				break;
			case "7":
				iv1.setImageDrawable(d1);
				iv2.setImageDrawable(d2);
				iv3.setImageDrawable(d2);
				iv4.setImageDrawable(d2);
				break;
			case "8":
				iv1.setImageDrawable(d2);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d1);
				break;
			case "9":
				iv1.setImageDrawable(d2);
				iv2.setImageDrawable(d1);
				iv3.setImageDrawable(d1);
				iv4.setImageDrawable(d2);
				break;
			default:
				return new LinearLayout(c);
		}
		View[] va = {iv1,space(),iv2,space(),iv3,space(),iv4};
		for(View v : va)
			ll.addView(v);
		return ll;
	}
	
	Handler h = new Handler();
	Runnable r;
	
	View space(){
		View v = new View(c);
		v.setLayoutParams(new LinearLayout.LayoutParams(
							  cm.menuItemPadding(),
							  cm.menuItemPadding()));
		return v;
	}
	
	public LinearLayout analogClock(){
		final LinearLayout ml = new LinearLayout(c);
		ml.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ml.setOrientation(LinearLayout.VERTICAL);
		ml.setGravity(Gravity.CENTER_HORIZONTAL);
		ml.setPadding(0,cm.menuItemPadding()*2,0,cm.menuItemPadding()*2);
		/*final LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER_HORIZONTAL);
		/*final TextClock tc = new TextClock(c);
		tc.setLayoutParams(new LinearLayout.LayoutParams(1,1));
		ll.addView(tc);
		tc.setFormat12Hour("h:mm");
		tc.setFormat24Hour("HH:mm");*/
		final TextView tv = new TextView(c);
		tv.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.WRAP_CONTENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		tv.setText(sdf.format(new Date()));
		tv.setTextColor(0xFFFFFFFF);
		tv.setTextSize(cm.textSize2());
		tv.setTypeface(cm.tf());
		tv.setPadding(0,cm.menuItemPadding(),0,0);
		/*tc.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4){}

				@Override
				public void onTextChanged(CharSequence p1, int p2, int p3, int p4){
					ll.removeAllViews();
					AnalogClock acl = new AnalogClock(c);
					acl.setLayoutParams(new LinearLayout.LayoutParams(
											cm.getDeviceWidth()/2,
											(int)(cm.getDeviceWidth()/2.5f)));
					ll.addView(acl);
					tv.setText(sdf.format(new Date()));
					h.postDelayed((r = new Runnable(){
									  public void run(){
										  ll.addView(tc);
									  }
								  }),100);
				}

				@Override
				public void afterTextChanged(Editable p1){}
			});*/
		//ml.addView(ll);
		AnalogClock acl = new AnalogClock(c);
		acl.setLayoutParams(new LinearLayout.LayoutParams(
								cm.getDeviceWidth()/2,
								(int)(cm.getDeviceWidth()/2.5f)));
		ml.addView(acl);
		ml.addView(tv);
		return ml;
	}
	
}
