package org.blinksd.launcher.modules;

import android.app.*;
import android.content.*;
import android.view.*;

public class FullScreen {
	
	Context c;
	
	public FullScreen(Context ctx){
		c = ctx;
	}
	
	// This snippet hides the system bars.
	public void hideSystemUI() {
		// Set the IMMERSIVE flag.
		// Set the content to appear under the system bars so that the content
		// doesn't resize when the system bars hide and show.
		View decorView = ((Activity)c).getWindow().getDecorView();
		decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
            | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}

// This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
	public void showSystemUI() {
		View decorView = ((Activity)c).getWindow().getDecorView();
		decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
	}
}
