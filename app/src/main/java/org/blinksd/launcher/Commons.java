package org.blinksd.launcher;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import org.blinksd.launcher.*;

public class Commons{
	
	Context c;
	DisplayMetrics dm;
	
	public Commons(Context ctx){
		c = ctx;
		dm = new DisplayMetrics();
		((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(dm);
	}
	
	public int getDeviceHeight(boolean sbar){
		int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
		int sbh = resourceId > 0 ? c.getResources().getDimensionPixelSize(resourceId) : 0;
		return sbar ? sbh+dm.heightPixels : dm.heightPixels;
	}
	
	public float getDensity(){
		return dm.density;
	}
	
	public int getDeviceWidth(){
		return (int)(dm.widthPixels/dm.density);
	}

	public int menuItemSize(){
		return (int)(actionBarHeight()*0.75);
	}

	public int menuItemPadding(){
		return (int)(actionBarHeight()*0.0625);
	}

	public int actionBarHeight(){
		return (getDeviceHeight(false)/100)*7;
	}

	public int textSize1(){
		return (int)((actionBarHeight()*0.45)/dm.density);
	}

	public int textSize2(){
		return (int)(textSize1()*0.7);
	}

	public int textSize3(){
		return (int)(textSize2()*0.75);
	}
	
	public int launcherItemSize(){
		return getDeviceHeight(false)/6;
	}
	
	public Dialog getCurrentDialog(){
		return d;
	}

	private Dialog d;
	
	public void createSelectionDialog(String title, View... views, View.OnClickListener... ocl){
		LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   LinearLayout.LayoutParams.MATCH_PARENT));
		ll.setBackground(createRadiusDrawable(0xFF222222,16));
		ll.setPadding(36,36,36,36);
		ll.setOrientation(LinearLayout.VERTICAL);

		if(title != "" && title != null){
			TextView tt = new TextView(c);
			tt.setLayoutParams(new LinearLayout.LayoutParams(
								   LinearLayout.LayoutParams.MATCH_PARENT,
								   LinearLayout.LayoutParams.MATCH_PARENT));
			tt.setText(title);
			tt.setSingleLine(true);
			tt.setPadding(menuItemPadding(),0,0,menuItemPadding()*3);
			tt.setTextSize(textSize2());
			tt.setTypeface(tf());
			tt.setGravity(Gravity.CENTER_HORIZONTAL);
			tt.setTextColor(0xFFFFFFFF);
			ll.addView(tt);
		}
		
		for(View x : views)
			ll.addView(x);

		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);
		LinearLayout ll2 = new LinearLayout(c);
		ll2.setLayoutParams(lp);
		ll2.setPadding(0,menuItemPadding()*2,menuItemPadding()*2,0);
		ll2.setGravity(Gravity.RIGHT);
		if(ocl.length > 1){
			TextView bt2 = createButton(c.getResources().getString(android.R.string.cancel), ocl[1]);
			ll2.addView(bt2);
			View v = new View(c);
			v.setLayoutParams(new LinearLayout.LayoutParams(
								  32,LinearLayout.LayoutParams.MATCH_PARENT));
			ll2.addView(v);
		} if(ocl.length >= 1){
			TextView bt = createButton(c.getResources().getString(android.R.string.ok), ocl[0]);
			ll2.addView(bt);
		}
		ll.addView(ll2);
		d = new Dialog(c);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.getWindow().setBackgroundDrawable(createRadiusDrawable(0x00123456,16));
		d.setContentView(ll);
		d.show();
		if(ocl.length < 1)
			h.postDelayed(r,5000);
	}

	RelativeLayout selectionItem(final String title, int icon, View.OnClickListener ocl){
		final RelativeLayout rl = new RelativeLayout(c);
		rl.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   (int)(menuItemSize()*1.75)));
		rl.setGravity(Gravity.CENTER_VERTICAL);
		rl.setPadding(menuItemPadding()*3,menuItemPadding()*3,menuItemPadding()*3,menuItemPadding()*3);
		ImageView iv = new ImageView(c);
		iv.setLayoutParams(new RelativeLayout.LayoutParams(menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT));
		iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
		iv.setPadding(menuItemPadding(),menuItemPadding(),menuItemPadding(),menuItemPadding());
		iv.setImageResource(icon);
		rl.addView(iv);
		LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new RelativeLayout.LayoutParams(
							   RelativeLayout.LayoutParams.MATCH_PARENT,
							   RelativeLayout.LayoutParams.MATCH_PARENT));
		ll.setPadding(menuItemSize()+(menuItemPadding()*2),0,menuItemSize()+(menuItemPadding()*2),0);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setGravity(Gravity.CENTER_VERTICAL);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.MATCH_PARENT);
		TextView t1 = new TextView(c);
		t1.setLayoutParams(lp);
		t1.setGravity(Gravity.CENTER_VERTICAL);
		t1.setTypeface(tf());
		t1.setTextSize(textSize2());
		t1.setText(title);
		t1.setSingleLine();
		t1.setTextColor(0xFFFFFFFF);
		ll.addView(t1);
		rl.addView(ll);
		rl.setOnClickListener(ocl);
		rl.setOnTouchListener(new View.OnTouchListener(){
				@Override
				public boolean onTouch(View p1, MotionEvent p2){
					rl.setBackgroundDrawable(p2.getAction() == p2.ACTION_DOWN || p2.getAction() == p2.ACTION_SCROLL 
										  ? createRadiusDrawable(drawableColor,200f) : 
										  p2.getAction() == p2.ACTION_UP || p2.getAction() == p2.ACTION_OUTSIDE ||
										  p2.getAction() == p2.ACTION_HOVER_EXIT || p2.getAction() == p2.ACTION_CANCEL
										  ? new ColorDrawable(0x00123456) : createRadiusDrawable(drawableColor,200f));
					return false;
				}
			});
		return rl;
	}
	
	public Typeface tf(){
		return Typeface.create("sans-serif-light",0);
	}
	
	private TextView createButton(String str, View.OnClickListener ocl){
		final TextView bt = new TextView(c);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.WRAP_CONTENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);
		bt.setLayoutParams(lp);
		bt.setTextSize(18);
		bt.setTextColor(0xFFFFFFFF);
		bt.setPadding(menuItemPadding()*4,menuItemPadding(),menuItemPadding()*4,menuItemPadding());
		bt.setText(str.toUpperCase());
		bt.setGravity(Gravity.CENTER);
		bt.setOnClickListener(ocl);
		bt.setOnTouchListener(new View.OnTouchListener(){
			public boolean onTouch(View p1, MotionEvent p2){
				bt.setBackgroundDrawable(p2.getAction() == p2.ACTION_DOWN || p2.getAction() == p2.ACTION_SCROLL 
										 ? createRadiusDrawable(drawableColor,200f) : 
										 p2.getAction() == p2.ACTION_UP || p2.getAction() == p2.ACTION_OUTSIDE ||
										 p2.getAction() == p2.ACTION_HOVER_EXIT || p2.getAction() == p2.ACTION_CANCEL
										 ? new ColorDrawable(0x00123456) : createRadiusDrawable(drawableColor,200f));
				return false;
			}
		});
		return bt;
	}
	
	public Drawable createRadiusDrawable(int color, float radius, int...stroke){
		GradientDrawable gd = new GradientDrawable();
		gd.setColors(new int[]{color,color});
		if(stroke.length > 0 && stroke.length == 2)
			gd.setStroke(stroke[0],stroke[1]);
		gd.setCornerRadii(new float[]{radius,radius,radius,radius,radius,radius,radius,radius});
		return gd;
	}
	
	int drawableColor = 0x44FFFFFF;
	
	private Handler h = new Handler();
	private Runnable r = new Runnable(){
		public void run(){
			d.cancel();
		}
	};
	
}
