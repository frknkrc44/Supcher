package org.blinksd.launcher;

import android.content.*;
import android.content.pm.*;
import android.graphics.drawable.*;
import android.net.*;
import java.io.*;
import java.util.*;

public class PackageList {

	Context c;
	private PackageManager pm;
	private List<PackageInfo> pi;
	String pkgdir;
	
	PackageList(Context ctx){
		c = ctx;
		pm = ctx.getPackageManager();
		pi = pm.getInstalledPackages(PackageManager.GET_META_DATA);
		pkgdir = "/sdcard/Android/data/"+ctx.getPackageName()+"/apks";
		ctrldir();
	}
	
	void ctrldir(){
		File x = new File(pkgdir);
		if(!x.exists())
			x.mkdirs();
	}
	
	public PackageInfo getPackage(int index){
		return pi.get(index);
	}

	public int getCount(){
		return pi.size();
	}

	public String getPackageName(int index){
		return getPackage(index).packageName;
	}

	public String[] getAllPackageNames(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getPackageName(i)+";");
		return sb.toString().split(";");
	}

	public String getAppName(int index){
		return getPackage(index).applicationInfo.loadLabel(pm).toString();
	}
	
	public Drawable getAppIcon(int index){
		return getPackage(index).applicationInfo.loadIcon(pm);
	}

	public String[] getAllAppNames(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getAppName(i)+";");
		return sb.toString().split(";");
	}

	public String getPackageLocation(int index){
		return getPackage(index).applicationInfo.sourceDir;
	}

	public String[] getAllPackageLocations(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getCount();i++)
			sb.append(getPackageLocation(i)+";");
		return sb.toString().split(";");
	}

	private String[] getAllLaunchableAppPkgNames(){
		String[] getAll = getAllPackageNames();
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != getAll.length;i++)
			if(pm.getLaunchIntentForPackage(getAll[i]) != null)
				sb.append(getAll[i]+"∆"+i+" ");
		return sb.toString().split(" ");
	}
	
	public String[] getAllLaunchableApps(){
		String[] t = getAllLaunchableAppPkgNames();
		StringBuilder sb = new StringBuilder();
		for(int i = 0;i != t.length;i++)
			if(!(t[i].split("∆")[0].equals(c.getPackageName())))
				sb.append(getAppName(Integer.parseInt((t[i].split("∆")[1])))
						+"∆"+t[i].split("∆")[1]+"∆"+t[i].split("∆")[0]+";");
		String[] x = sb.toString().split(";");
		Arrays.sort(x);
		return x;
	}
	
	public String[] getAllLaunchableAppNames(){
		String[] t = getAllLaunchableApps();
		StringBuilder sb = new StringBuilder();
		for(String x : t)
			sb.append(x.split("∆")[0]+";");
		return sb.toString().split(";");
	}
	
	public Drawable[] getAllLaunchableAppIcons(){
		String[] t = getAllLaunchableApps();
		Drawable[] ba = new Drawable[9999];
		for(int i = 0;i != t.length;i++){
			ba[i] = getAppIcon(Integer.parseInt((t[i].split("∆")[1])));
		}
		return ba;
	}
	
	public String[] getAllLaunchableAppPackageNames(){
		String[] t = getAllLaunchableApps();
		StringBuilder sb = new StringBuilder();
		for(String x : t)
			sb.append(x.split("∆")[2]+";");
		return sb.toString().split(";");
	}
	
	public String[] getAllLaunchableAppPackageIndexNumbers(){
		String[] t = getAllLaunchableApps();
		StringBuilder sb = new StringBuilder();
		for(String x : t)
			sb.append(x.split("∆")[1]+";");
		return sb.toString().split(";");
	}
	
	File w;
	
	public void pullAndSendAPK(String pkg) throws IOException {
		ctrldir();
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		List<ResolveInfo> apps = pm.queryIntentActivities(mainIntent, 0);
		for (ResolveInfo info : apps){
			if(info.activityInfo.applicationInfo.packageName.equals(pkg)){
				File f = new File(info.activityInfo.applicationInfo.publicSourceDir);
				w = new File(pkgdir+"/"+pkg+".apk");
				w.createNewFile();
				copyFile(f,w);
				break;
			}
		}
		if(w != null){
			Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND); 
			sharingIntent.setType("application/vnd.android.package-archive");
			sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(w));
			c.startActivity(Intent.createChooser(sharingIntent, "Share with"));
		}
	}
	
	private void copyFile(File sourceFile, File destFile) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(sourceFile);
            out = new FileOutputStream(destFile);
            byte[] buffer = new byte[1024];
            int part;
            while ((part = in.read(buffer)) > 0)
                out.write(buffer,0,part);
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            in.close();
            out.close();
        }
    }
	
}
